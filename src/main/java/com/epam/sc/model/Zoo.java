package com.epam.sc.model;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component("zooComponent")
public class Zoo {
    private List<Area> areas = new ArrayList<Area>();

    public void setAreas(final List<Area> areas) {
        this.areas.clear();
        this.areas.addAll(areas);
    }

    public List<Area> getAreas() {
        return Collections.unmodifiableList(areas);
    }

    @Override
    public String toString() {
        return "Zoo[" + areas + "]";
    }
}
