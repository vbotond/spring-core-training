package com.epam.sc.model;

public class Boar extends Animal {
    public Boar(final String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Boar['" + getName() + "']";
    }
}
