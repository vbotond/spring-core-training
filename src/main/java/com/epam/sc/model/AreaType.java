package com.epam.sc.model;

public enum AreaType {
    CAGE,
    DESERT,
    ROCKY;
}
