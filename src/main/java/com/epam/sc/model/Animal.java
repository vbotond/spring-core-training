package com.epam.sc.model;

import org.springframework.beans.factory.annotation.Required;

public abstract class Animal {
    private String name;
    private Animal spouse;

    public Animal(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Required
    public void setSpouse(final Animal spouse) {
        this.spouse = spouse;
    }

    public Animal getSpouse() {
        return spouse;
    }
}
