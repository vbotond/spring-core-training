package com.epam.sc.model;

public class Deer extends Animal {
    public Deer(final String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Deer['" + getName() + "']";
    }
}
