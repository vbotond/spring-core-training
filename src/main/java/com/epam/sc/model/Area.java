package com.epam.sc.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Area {
    private static Logger logger = LoggerFactory.getLogger(Area.class);

    private String id;
    private AreaType type;
    private List<Animal> animals = new ArrayList<Animal>();

    public Area(final String id, final AreaType type) {
        this.id = id;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public AreaType getType() {
        return type;
    }

    public void setAnimals(final List<Animal> animals) {
        this.animals.clear();
        this.animals.addAll(animals);
    }

    public List<Animal> getAnimals() {
        return Collections.unmodifiableList(animals);
    }

    @Override
    public String toString() {
        return "Area[" + id + "," + type + "," + animals + "]";
    }
}
