package com.epam.sc.model;

public class WhiteFox extends Fox {
    public WhiteFox(final String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "WhiteFox['" + getName() + "'," + getSpouse() + "]";
    }
}
