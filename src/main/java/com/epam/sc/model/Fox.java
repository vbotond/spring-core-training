package com.epam.sc.model;

public class Fox extends Animal {
    public Fox(final String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Fox['" + getName() + "'," + getSpouse() + "]";
    }
}
