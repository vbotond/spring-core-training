package com.epam.sc.converter;

import com.epam.sc.model.Area;
import com.epam.sc.model.AreaType;
import org.springframework.core.convert.converter.Converter;

public class AreaCoverter implements Converter<String, Area> {
    @Override
    public Area convert(final String text) {
        final String[] values = text.split("\\|");
        final Area area = new Area(values[0], AreaType.valueOf(values[1]));
        return area;
    }
}
