package com.epam.sc.postprocessor;

import com.epam.sc.model.Fox;
import com.epam.sc.model.WhiteFox;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class WhiteFoxPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(final Object bean, final String beanName) throws BeansException {
        return bean;
    }

    @Override public Object postProcessAfterInitialization(final Object bean, final String beanName) throws BeansException {
        Object result = bean;
        if (bean instanceof Fox) {
            final Fox fox = (Fox) bean;
            if (fox.getName().indexOf("White") >= 0) {
                result = new WhiteFox(fox.getName());
                ((Fox) result).setSpouse(fox.getSpouse());
            }
        }
        return result;
    }
}
