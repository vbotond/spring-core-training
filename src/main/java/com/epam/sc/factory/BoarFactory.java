package com.epam.sc.factory;

import com.epam.sc.model.Boar;
import org.springframework.beans.factory.FactoryBean;

public class BoarFactory implements FactoryBean<Boar> {
    private String name;
    private int count;

    public BoarFactory(final String name) {
        this.name = name;
    }

    @Override
    public Boar getObject() throws Exception {
        count++;
        return new Boar("New born boar #" + count + " (" + name + ")");
    }

    @Override
    public Class<?> getObjectType() {
        return Boar.class;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }
}
