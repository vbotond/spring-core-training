package com.epam.sc.propertyeditor;

import com.epam.sc.model.Area;
import com.epam.sc.model.AreaType;

import java.beans.PropertyEditorSupport;

public class AreaPropertyEditor extends PropertyEditorSupport {
    @Override
    public void setAsText(final String text) {
        final String[] values = text.split("\\|");
        final Area area = new Area(values[0], AreaType.valueOf(values[1]));
        setValue(area);
    }
}
