package com.epam.sc;

import com.epam.sc.model.Animal;
import com.epam.sc.model.Area;
import com.epam.sc.model.AreaType;
import com.epam.sc.model.Fox;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ComponentScan(basePackages = {"com.epam.sc"})
@Profile("profile2")
public class AppConfig2 {
    @Bean
    @Scope("prototype")
    public Fox johnny() {
        return new Fox("Johnny 2 'fire' Fox (" + System.nanoTime() + ")");
    }

    @Bean
    public Area desert1() {
        final Area result = new Area("Desert #1", AreaType.DESERT);
        final List<Animal> animals = new ArrayList<Animal>();
        animals.add(johnny());
        result.setAnimals(animals);
        return result;
    }

    @Bean
    public Area desert2() {
        final Area result = new Area("Desert #2", AreaType.DESERT);
        final List<Animal> animals = new ArrayList<Animal>();
        animals.add(johnny());
        result.setAnimals(animals);
        return result;
    }
}
