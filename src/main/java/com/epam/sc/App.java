package com.epam.sc;

import com.epam.sc.model.Animal;
import com.epam.sc.model.Area;
import com.epam.sc.model.Zoo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;

public class App {
    private static Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(AppConfig.class, AppConfig2.class);

        final Map<String, Animal> animals = context.getBeansOfType(Animal.class);
        for (Map.Entry<String, Animal> e : animals.entrySet()) {
            logger.info("[" + e.getKey() + "] - " + e.getValue());
        }

        final Map<String, Area> areas = context.getBeansOfType(Area.class);
        for (Map.Entry<String, Area> e : areas.entrySet()) {
            logger.info("[" + e.getKey() + "] - " + e.getValue());
        }

        final Map<String, Zoo> zoos = context.getBeansOfType(Zoo.class);
        for (Map.Entry<String, Zoo> z : zoos.entrySet()) {
            logger.info("[" + z.getKey() + "] - " + z.getValue());
        }
    }
}
